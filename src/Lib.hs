module Lib
  ( proc
  , CharTemplate
  , TemplateItem(..)
  , parseTemplate
  , parseHole
  , HoleName(..)
  ) where

import Prelude (FilePath, IO, Char, ($))

import qualified Prelude as Sys

-- import Temp (Template, parseTemplate)

proc :: FilePath -> [FilePath] -> IO ()
proc templateFile _catalogs = do
  templateSource <- Sys.readFile templateFile

  Sys.print (templateFile, templateSource)

  let template = parseTemplate templateSource []
  Sys.print template

  -- Sys.print helloTemplate
  -- Sys.print ("catalogs", catalogs)

---------------- Temp.hs ---------------
-- module Temp (Template(..), parseTemplate) where

-- doTheThing = theThing source [] []

-- TODO: rewrite in takeWhileP

parseTemplate :: [Char] -> CharTemplate -> CharTemplate
parseTemplate source acc =
  case source of
    [] ->
      acc

    '{' : '{' : next ->
      let
        (rest, holeName) = parseHole next []
      in
        parseTemplate rest (Hole holeName : acc)

      --case parseHole next [] of
        --(a, b) -> parseTemplate a (Hole b : acc)

    _ ->
      let
        (rest, chunk) = parseChunk source []
      in
        parseTemplate rest (Chunk chunk : acc)

parseChunk :: [Char] -> [Char] -> ([Char], [Char])
parseChunk input acc =
  case input of
    [] ->
      ([], acc)
    '{' : '{' : rest ->
      (rest, acc)
    ch : rest ->
        parseChunk rest (ch : acc)

parseHole :: [Char] -> [Char] -> ([Char], HoleName)
parseHole input holeAcc =
  case input of
    '}' : '}' : rest ->
      (rest, HoleName $ Sys.reverse holeAcc)

    ho : rest ->
      parseHole rest (ho : holeAcc)

    [] ->
      Sys.error "Hole parsing ended abruptly"

-- parseHole :: [Char] -> CharTemplate
-- parseHole source =
--   case source of
--     ' ' : next -> parseHole next
--     '}' : '}' : next -> parseTemplate next
--     ch : next -> HoleName [ch] -- parseHole next

-- 'h' : 'e' : .... : []

type CharTemplate = [TemplateItem Char]

data TemplateItem a
  = Chunk [a]
  | Hole HoleName
  deriving (Sys.Show)

newtype HoleName = HoleName [Char]
  deriving (Sys.Show)
