module Parser where

import Prelude

takeWhileP :: ([a] -> Bool) -> [a] -> ([a], [a])
takeWhileP = go []
  where
    go :: [a] -> ([a] -> Bool) -> [a] -> ([a], [a])
    go acc f xs =
      case xs of
        [] ->
          (acc, [])
        x : xs' ->
          case f xs of
            True ->
              go (x : acc) f xs'
            False ->
              (acc, xs)
