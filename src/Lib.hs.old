{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE InstanceSigs #-}

module Lib where
    -- ( someFunc
    -- , UnitType(..)
    -- , FooBar(..)
    -- , foo
    -- , bar
    -- ) where

someFunc :: IO ()
someFunc = putStrLn "hello world"

-- sum-type
data UnitType = UnitConstr

unitThing :: UnitType
unitThing = UnitConstr

-- sum-type
data FooBar
  = Foo
  | Bar
  deriving (Show)

-- product-type
data Spam = Spam UnitType FooBar

spam1 :: Spam
spam1 = Spam UnitConstr Foo

spam2 :: Spam
spam2 = Spam UnitConstr Bar

type UFbPair = (UnitType, FooBar)

fromSpam2 :: Spam -> MyPair UnitType FooBar
fromSpam2 (Spam ut fb) = (ut, fb)

type MyPair a b = (a, b)

mkPair :: a -> b -> (a, b)
mkPair a b = (a, b)

mkPair2 :: a -> b -> MyPair a b
mkPair2 = mkPair

data ThePair a b
  = Forward  a b
  | Backward b a

data ThePairG a b where
  ForwardG  :: a -> b -> ThePairG a b
  BackwardG :: b -> a -> ThePairG a b

fw :: ThePair Int Bool
fw = Forward 1 True

bw :: ThePair Int Bool
bw = Backward True 1

fromSpam :: Spam -> UFbPair
fromSpam s =
  case s of
    Spam ut fb ->
      (ut, fb)

getA :: ThePair a b -> a
getA (Forward a _b) = a
getA (Backward _b a) = a

getB :: ThePair a b -> b
getB g =
  case g of
    Forward _ b -> b
    Backward b _ -> b

thePair :: ThePair a b -> (a, b)
thePair tp =
  case tp of
    Forward a b -> (a, b)
    Backward b a -> (a, b)


--------------------------------
-- generalized algebraic data type
data Things
  where
    Thingy :: Int -> Things
    Blob   ::        Things

th :: Things
th = Thingy 42

bl :: Things
bl = Blob

--------------------------------

foo :: FooBar
foo = Foo

bar :: FooBar
bar = Bar

flipFB :: FooBar -> FooBar
flipFB Foo = Bar
flipFB Bar = Foo

foobar :: FooBar -> Int
foobar Foo = 0
foobar Bar = 1

data Result
  = Okay Int
  | Failed

canDivide :: Int -> Int -> Result
canDivide    _      0    = Failed
canDivide    _      x    = Okay x

whatevs :: FooBar -> ()
whatevs x =
  case x of
    Foo ->
      ()
    Bar ->
      ()

huh :: ()
huh =
  -- whatevs Foo
  case Foo of
    _ ->
      ()

takesUnit :: () -> Int
takesUnit () = 0

xxx :: Int
xxx = takesUnit huh

sum3 :: Int -> Int -> Int -> Int
sum3 num1 num2 num3 = num1 + num2 + num3

data Maybe' a
  = Nothing'
  | Just' a

class Mmap (t :: * -> *) where
  mmap :: (a -> b) -> t a -> t b

instance Mmap (Maybe :: * -> *) where
  mmap :: (a -> b) -> Maybe a -> Maybe b
  mmap _ Nothing = Nothing
  mmap z (Just a) = Just (z a)

instance Mmap List where
  mmap :: (a -> b) -> List a -> List b
  mmap _f Empty = Empty
  mmap ff (Head x xs) = Head (ff x) (mmap ff xs)

instance Functor Maybe' where
  fmap :: (a -> b) -> Maybe' a -> Maybe' b
  fmap _g Nothing' = Nothing'
  fmap g (Just' a) = Just' (g a)

instance Applicative Maybe' where
  pure :: a -> Maybe' a
  pure = Just'

  (<*>) :: Maybe' (a -> b) -> Maybe' a -> Maybe' b
  Nothing' <*> _mx = Nothing'
  Just' g <*> mx = fmap g mx

optionalAdd2 :: Maybe Int -> Int -> Int
optionalAdd2 ma b =
  case ma of
    Just a ->
      a + b
    Nothing ->
      b

safeDivide :: Float -> Float -> Maybe Float
safeDivide _ 0 = Nothing
safeDivide x y = Just (x / y)

divisible :: Float -> Float -> Bool
divisible x y = case safeDivide x y of
  Nothing -> False
  Just _  -> True

divide :: Float -> Float -> Float
divide x y =
  if divisible x y then
    x / y
  else
    0

data Either' a b
  = Left' a
  | Right' b

divideE :: Float -> Float -> Either DivError Float
divideE _ 0 = Left DivZero
divideE x y = Right (x / y)

data DivError
  = DivZero
  -- deriving (Show)

showDivError :: DivError -> String
showDivError DivZero = "Dividing by zero, eh?"

-- class Show' a where
--   show' :: a -> String

instance Show DivError where
  show DivZero = "Dividing by zero, eh?"

class Add a where
  add :: a -> a -> a
  zero :: a

instance Add Int where
  add a b = a + b
  zero = 0

instance Add P where
  add a b = plus a b
  zero = Z

showIsh :: Show a => a -> String
showIsh x = show x

i :: a -> a
i x = x

c :: a -> b -> a
c x = \_y -> x

f :: a
f = f

data P
  = Z
  | S P

instance Show P where
  show Z = "0"
  show (S a) = "1 + " <> show a

thre :: P
thre = S (S (S Z))

plus :: P -> P -> P
plus Z b = b
plus (S a) b = plus a (S b)

toInt :: P -> Int
toInt Z = 0
toInt (S a) = (+) 1 (toInt a)

intTo :: Int -> P
intTo 0 = Z
intTo n = S (intTo (n - 1))

infinity :: P
infinity = S infinity



data List a
  = Empty            -- []
  | Head a (List a)  --
  deriving (Show)

empty :: List a
empty = Empty

single :: a -> List a
single x = Head x Empty

couple :: a -> a -> List a
couple x y = Head x (single y)

len :: List a -> Int
len Empty = 0
len (Head _ x) = len x + 1

summ :: Add a => List a -> a
summ Empty = zero
summ (Head x xs) = add x (summ xs)

pr :: Show a => a -> IO Int
pr x = fmap unitToInt $ putStrLn (show x)
  where
    unitToInt :: () -> Int
    unitToInt () = length (show x)

mainIsh :: IO ()
mainIsh = do
  this <- pr True
  print this

echo :: IO [Char]
echo = do
  x <- getLine
  putStrLn x
  _r <- random
  pure (x :: [Char]) :: IO [Char]

random :: IO Char
random = pure '!'
