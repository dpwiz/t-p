module Main where

import Prelude (IO, putStrLn)

import System.Environment (getArgs)

import Lib (proc)

-- stuff-exe
main :: IO ()
main = do
  args <- getArgs
  -- print args
  case args of
    [] ->
      putStrLn "usage: template.in.txt [catalog.src ..]"
    tpl : cats ->
      proc tpl cats
