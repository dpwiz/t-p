import Prelude -- (Char, IO, pure, show, fail, ($), putStrLn)

import Lib (proc, parseHole, HoleName(..), TemplateItem(..))

import Parser (takeWhileP)

main :: IO ()
main = do
  testParser
  testHole
  testProc

isUnits :: [()] -> Bool
isUnits xs =
  case xs of
    [] -> False
    () : _ -> True

isTrue :: [Bool] -> Bool
isTrue xs =
  case xs of
    [] -> False
    x : _ -> x

testParser :: IO ()
testParser = do
  case takeWhileP isUnits [] of
    ([], []) ->
      pure ()
    a ->
      fail $ show a

  case takeWhileP isUnits [(), ()] of
    ([(), ()], []) ->
      pure ()
    a ->
      fail $ show a

  case takeWhileP isTrue [True, True, False, False] of
    ([True, True], [False, False]) ->
      pure ()
    a ->
      fail $ show a

  let
    startsWithHello xs =
      case xs of
        'h':'e':'l':'l':'o':_ -> True
        _ -> False

  case takeWhileP startsWithHello "hello there" of
    ("h", "ello there") ->
      pure ()
    a ->
      fail $ show a

  case takeWhileP startsWithHello "why, hello" of
    ("why, ", "hello") ->
      pure ()
    a ->
      fail $ show a

testHole :: IO ()
testHole = do
  case parseHole "hoho}}rere" [] of
    ("rere", HoleName "hoho") ->
      putStrLn "great success"
    a ->
      fail $ show a

testProc :: IO ()
testProc = proc "test/1.in.txt" ["test/1.cat"]

_helloTemplate :: [TemplateItem Char]
_helloTemplate =
  [ Chunk "hello: " -- same: ['h','e','l','l','o','!']
  , Hole (HoleName "hole 1")
  , Chunk "\n"
  , Chunk "bye.\n"
  ]
